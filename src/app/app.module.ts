import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentsDataImpl2Service } from './service/students-data-impl2.service';


@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {provide: StudentService, useClass: StudentsDataImpl2Service}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
