import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import { Student } from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class StudentsDataImpl2Service extends StudentService{
  constructor() { 
    super();
     }
 
   getStudents() : Observable<Student[]>{
     return of(this.students);
   };
   students: Student[] = [{
     'id': 5,
     'studentId': '602115005',
     'name': 'Chanikan',
     'surname': 'Meen',
     'gpa': 2.73
   }];
 }
 